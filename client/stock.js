ViewModel.addBinding({
  name: 'readonly',
  autorun(bindArg) {
    const isReadOnly = bindArg.getVmValue();
    if (isReadOnly === 'readonly') {
      bindArg.element.attr('readonly', 'readonly');
    } else {
      bindArg.element.removeAttr('readonly');
    }
  }
});

Template.stock.viewmodel({
  poliol: "",
  copolimero: "",
  cloruro: "",
  tdi: "",
  amina: "",
  silicona595: "",
  silicona585: "",
  T9: "",
  idActual: "",
  estaModificando: false,
  readOnly: "readonly",
  actualizar: function(id) {
    if (id !== '' && this.poliol() !== '' && this.copolimero() !== '' &&
        this.cloruro() !== '' && this.tdi() !=='' && this.amina() !== '' && this.silicona585() !== '' &&
        this.silicona595() !== '' && this.T9() !== '') {

      var stock = {
        poliol: (!_.isString(this.poliol())) ? this.poliol() : parseFloat(this.poliol().replace(/,/g, '.')),
        copolimero: (!_.isString(this.copolimero())) ? this.copolimero() : parseFloat(this.copolimero().replace(/,/g, '.')),
        cloruro: (!_.isString(this.cloruro())) ? this.cloruro() : parseFloat(this.cloruro().replace(/,/g, '.')),
        tdi: (!_.isString(this.tdi())) ? this.tdi() : parseFloat(this.tdi().replace(/,/g, '.')),
        amina: (!_.isString(this.amina())) ? this.amina() : parseFloat(this.amina().replace(/,/g, '.')),
        silicona595: (!_.isString(this.silicona595())) ? this.silicona595() : parseFloat(this.silicona595().replace(/,/g, '.')),
        silicona585: (!_.isString(this.silicona585())) ? this.silicona585() : parseFloat(this.silicona585().replace(/,/g, '.')),
        T9: (!_.isString(this.T9())) ? this.T9() : parseFloat(this.T9().replace(/,/g, '.'))
      };

      var result = Stock.update({_id: id}, {$set: stock});

      if (result) {
        Materialize.toast("Stock actualizado correctamente.", 4000);
        this.estaModificando(false);
        this.readOnly('readonly');
      } else {
        Materialize.toast("El stock no se puedo actualizar.", 4000);
      }

    } else {
      Materialize.toast("Debe completar todos los campos.", 4000);
    }
  },
  habilitar: function() {
    this.estaModificando(true);
    this.readOnly('');
  }
});