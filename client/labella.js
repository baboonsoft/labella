Template.home.viewmodel({
  hayBloques: function() {
    return Insumos.find().count() > 0;
  }
});
Template.main.viewmodel({
  onRendered: function() {
    $('.datepicker').pickadate({
      selectMonths: true,
      selectYears: 2,
      format: "dd-mm-yyyy"
    });
    this.reset();
  },
	tiposDeBloque: function() {
    return Insumos.find({}).fetch();
  },
  bloqueSeleccionado: "",
  textoPorDefecto: "Seleccione un bloque",
  cantidad: "",
  fecha: "",
  viendoDetalle: false,
  //Detalle parametros
  tipoBloqueDetalle: "",
  fechaDetalle: "",
  cantidadDetalle: "",
  poliol: "",
  copolimero: "",
  cloruro: "",
  tdi: "",
  amina: "",
  silicona595: "",
  silicona585: "",
  T9: "",
  //Filtros
  fechaFiltro: "",
  insumos: function() {
		return Insumos.find({tipoDeBloque : this.bloqueSeleccionado()});
	},
  producciones: function() {
    if (this.fechaFiltro() !== "") {
      return Producciones.find({fechaProduccion: this.fechaFiltro()});
    } else {
      return Producciones.find({});
    }
  },
  traerTodos: function() {
    this.fechaFiltro("");
  },
	buscarPorBloque: function() {
		var result = Insumos.find({"tipoDeBloque":"Espuma 10kg"}).fetch();
	},
  validarStock: function(poliolTot, copolimeroTot, cloruroTot, tdiTot, aminaTot, silicona595Tot, silicona585Tot, T9Tot) {
    var resultado = Stock.findOne({}),
        poliolTot = resultado.poliol - poliolTot,
        copolimeroTot = resultado.copolimero - copolimeroTot,
        cloruroTot = resultado.cloruro - cloruroTot,
        tdiTot = resultado.tdi - tdiTot,
        aminaTot = resultado.amina - aminaTot,
        silicona595Tot = resultado.silicona595 - silicona595Tot,
        silicona585Tot = resultado.silicona585 - silicona585Tot,
        T9Tot = resultado.T9 - T9Tot,
        stockMensaje = "No hay suficiente stock de: ",
        hayStock = true;

    if (poliolTot <= 0) {
      hayStock = false;
      stockMensaje += '* POLIOL ';
    }

    if (copolimeroTot <= 0) {
      hayStock = false;
      stockMensaje += '* COPOLIMERO ';
    }

    if (cloruroTot <= 0) {
      hayStock = false;
      stockMensaje += '* CLORURO ';
    }

    if (tdiTot <= 0) {
      hayStock = false;
      stockMensaje += '* TDI ';
    }

    if (aminaTot <= 0) {
      hayStock = false;
      stockMensaje += '* AMINA ';
    }

    if (silicona595Tot <= 0) {
      hayStock = false;
      stockMensaje += '* SILICONA595 ';
    }

    if (silicona585Tot <= 0) {
      hayStock = false;
      stockMensaje += '* SILICONA585 ';
    }

    if (T9Tot <= 0) {
      hayStock = false;
      stockMensaje += '* T9 ';
    }

    if (!hayStock) {
      Materialize.toast(stockMensaje, 10000);
    } else { // actualiza stock
      var stock = {
        poliol: poliolTot,
        copolimero: copolimeroTot,
        cloruro: cloruroTot,
        tdi: tdiTot,
        amina: aminaTot,
        silicona595: silicona595Tot,
        silicona585: silicona585Tot,
        T9: T9Tot
      }
      var result = Stock.update({_id: resultado._id}, {$set: stock});
      if (!result) {
        Materialize.toast("No se pudo actualizar el stock", 10000);
      }
    }

    return hayStock;

  },
  crearProduccion: function() {
    if (this.bloqueSeleccionado() !== "" && this.cantidad() !== '' && this.fecha() !== '') {
      var resultado = Insumos.find({"tipoDeBloque": this.bloqueSeleccionado()}).fetch();
      var poliolTot = 0, copolimeroTot = 0, cloruroTot = 0, tdiTot = 0, aminaTot = 0, silicona595Tot = 0, silicona585Tot = 0, T9Tot = 0;

      _.each(resultado, function (value) {
        poliolTot = value.poliol * this.cantidad();
        copolimeroTot = value.copolimero * this.cantidad();
        cloruroTot = value.cloruro * this.cantidad();
        tdiTot = value.tdi * this.cantidad();
        aminaTot = value.amina * this.cantidad();
        silicona595Tot = value.silicona595 * this.cantidad();
        silicona585Tot = value.silicona585 * this.cantidad();
        T9Tot = value.T9 * this.cantidad();
      }, this);

      if (this.validarStock(poliolTot, copolimeroTot, cloruroTot, tdiTot, aminaTot, silicona595Tot, silicona585Tot, T9Tot)) {

        var date = this.fecha();
        date = date.split("-");
        date = new Date(date[2], date[1] - 1, date[0]);
        date.setMinutes(0);
        date.setSeconds(0);
        date.setMilliseconds(0);

        var produccion = {
          fechaProduccion: date,
          tipoDeBloque: this.bloqueSeleccionado(),
          cantidad: parseInt(this.cantidad()),
          poliol: poliolTot,
          copolimero: copolimeroTot,
          cloruro: cloruroTot,
          tdi: tdiTot,
          amina: aminaTot,
          silicona595: silicona595Tot,
          silicona585: silicona585Tot,
          T9: T9Tot
        };

        var result = Producciones.insert(produccion, {autoConvert: false});
        if (result) {
          Materialize.toast("Produccion guardada correctamente. Stock actualizado", 6000);
          this.reset();
        } else {
          Materialize.toast("La produccion no se pudo crear, hubo un error.", 4000);
        }
      } else {
        Materialize.toast("No hay suficiente stock para guardar esta produccion, actualice el stock.", 10000);
      }
    } else {
      Materialize.toast("Por favor, seleccione un bloque y complete los campos.", 4000);
    }

  },
  salirDetalle: function() {
    this.viendoDetalle(false);
  }
});

Template.item.viewmodel({
  verDetalle: function() {
    this.parent().viendoDetalle(true);
    this.parent().tipoBloqueDetalle(this.tipoDeBloque());
    this.parent().fechaDetalle(this.fechaProduccion());
    this.parent().cantidadDetalle(this.cantidad());
    this.parent().poliol(this.poliol());
    this.parent().copolimero(this.copolimero());
    this.parent().cloruro(this.cloruro());
    this.parent().tdi(this.tdi());
    this.parent().amina(this.amina());
    this.parent().silicona595(this.silicona595());
    this.parent().silicona585(this.silicona585());
    this.parent().T9(this.T9());
  },
  eliminar: function() {
    var result = confirm("Seguro que desea eliminar esta producción ?");
    if (result) {
      result = Producciones.remove({_id: this._id()});
      if (result) {
        Materialize.toast("Produccion eliminada correctamente", 3000);
      } else {
        Materialize.toast("Hubo un problema para eliminar esta produccion", 3000);
      }
    }
  }
});
