Template.producciones.viewmodel({
  onRendered: function() {
    $('.datepicker').pickadate({
      selectMonths: true,
      selectYears: 15,
      format: "dd-mm-yyyy"
    });
    this.reset();
  },
  fechaInicio: "",
  fechaFin: "",
  poliol: "",
  copolimero: "",
  cloruro: "",
  tdi: "",
  amina: "",
  silicona595: "",
  silicona585: "",
  T9: "",
  calcularProduccion: function() {
    var poliol=0,
        copolimero=0,
        cloruro=0,
        tdi=0,
        amina=0,
        silicona595=0,
        silicona585=0,
        T9=0,
        fechaInicio = this.fechaInicio(),
        fechaFin = this.fechaFin();

    fechaInicio = fechaInicio.split("-");
    fechaInicio = new Date(fechaInicio[2],fechaInicio[1]-1,fechaInicio[0]);
    fechaFin = fechaFin.split("-");
    fechaFin = new Date(fechaFin[2],fechaFin[1]-1,fechaFin[0]);

    var resultado = Producciones.find({fechaProduccion: {$gte: fechaInicio, $lte: fechaFin}});
    resultado.forEach(function(value){
      poliol += value.poliol;
      copolimero += value.copolimero;
      cloruro += value.cloruro;
      tdi += value.tdi;
      amina += value.amina;
      silicona595 += value.silicona595;
      silicona585 += value.silicona585;
      T9 += value.poliol
    });

    this.poliol(poliol);
    this.copolimero(copolimero);
    this.cloruro(cloruro);
    this.tdi(tdi);
    this.amina(amina);
    this.silicona595(silicona595);
    this.silicona585(silicona585);
    this.T9(T9);
  },
  limpiarResultados: function() {
    this.poliol("");
    this.copolimero("");
    this.cloruro("");
    this.tdi("");
    this.amina("");
    this.silicona595("");
    this.silicona585("");
    this.T9("");
  },
  salirDetalle: function() {
    this.viendoDetalle(false);
  }
});