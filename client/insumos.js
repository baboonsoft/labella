Template.form.viewmodel({
  nombreBloque: "",
  poliol: "",
  copolimero: "",
  cloruro: "",
  tdi: "",
  amina: "",
  silicona595: "",
  silicona585: "",
  T9: "",
  idActual: "",
  estaModificando: false,
  bloques: function() {
    return Insumos.find({});
  },
  crearBloque: function() {
    if (this.nombreBloque() !== '' && this.poliol() !== '' && this.copolimero() !== '' &&
        this.cloruro() !== '' && this.tdi() !=='' && this.amina() !== '' && this.silicona585() !== '' &&
        this.silicona595() !== '' && this.T9() !== '') {
      
      var bloque = {
        tipoDeBloque: this.nombreBloque(),
        poliol: (!_.isString(this.poliol())) ? this.poliol() : parseFloat(this.poliol().replace(/,/g, '.')),
        copolimero: (!_.isString(this.copolimero())) ? this.copolimero() : parseFloat(this.copolimero().replace(/,/g, '.')),
        cloruro: (!_.isString(this.cloruro())) ? this.cloruro() : parseFloat(this.cloruro().replace(/,/g, '.')),
        tdi: (!_.isString(this.tdi())) ? this.tdi() : parseFloat(this.tdi().replace(/,/g, '.')),
        amina: (!_.isString(this.amina())) ? this.amina() : parseFloat(this.amina().replace(/,/g, '.')),
        silicona595: (!_.isString(this.silicona595())) ? this.silicona595() : parseFloat(this.silicona595().replace(/,/g, '.')),
        silicona585: (!_.isString(this.silicona585())) ? this.silicona585() : parseFloat(this.silicona585().replace(/,/g, '.')),
        T9: (!_.isString(this.T9())) ? this.T9() : parseFloat(this.T9().replace(/,/g, '.'))
      };
      var result = '';
      if (this.estaModificando()) {
        result = Insumos.update({_id: this.idActual()}, {$set: bloque});
      } else {
        result = Insumos.insert(bloque, {autoConvert: false});
      }

      if (result) {
        if (this.estaModificando()) {
          Materialize.toast("Bloque modificado correctamente.", 4000);
        } else {
          Materialize.toast("Bloque creado correctamente.", 4000);
        }
        this.reset();
      } else {
        Materialize.toast("El Bloque no se pudo crear hubo un error.", 4000);
      }
    } else {
      Materialize.toast("Debe completar todos los campos.", 4000);
    }
  }
});

Template.bloque.viewmodel({
  eliminar: function() {
    var result = confirm("Seguro que desea eliminar este bloque?");
    if (result) {
      result = Insumos.remove({_id: this._id()});
      if (result) {
        Materialize.toast("Bloque eliminado correctamente", 3000);
      } else {
        Materialize.toast("Hubo un problema para eliminar este bloque", 3000);
      }
    }
  },
  modificar: function() {
    this.parent().estaModificando(true);
    this.parent().nombreBloque(this.tipoDeBloque());
    this.parent().poliol(this.poliol());
    this.parent().copolimero(this.copolimero());
    this.parent().cloruro(this.cloruro());
    this.parent().tdi(this.tdi());
    this.parent().amina(this.amina());
    this.parent().silicona595(this.silicona595());
    this.parent().silicona585(this.silicona585());
    this.parent().T9(this.T9());
    this.parent().idActual(this._id());
  }
});