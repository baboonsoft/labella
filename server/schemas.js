var Schemas = {};

Schemas.Stock = new SimpleSchema({
	poliol: {
		type: Number,
		decimal: true
	},
	copolimero: {
		type: Number,
		decimal: true
	},
	cloruro: {
		type: Number,
		decimal: true
	},
	tdi: {
		type: Number,
		decimal: true
	},
	amina: {
		type: Number,
		decimal: true
	},
	silicona595: {
		type: Number,
		decimal: true
	},
	silicona585: {
		type: Number,
		decimal: true
	},
	T9: {
		type: Number,
		decimal: true
	}
});

Schemas.Insumo = new SimpleSchema({
	tipoDeBloque: {
		type: String
	},
	poliol: {
		type: Number,
		decimal: true
	},
	copolimero: {
		type: Number,
		decimal: true
	},
	cloruro: {
		type: Number,
		decimal: true
	},
	tdi: {
		type: Number,
		decimal: true
	},
	amina: {
		type: Number,
		decimal: true
	},
	silicona595: {
		type: Number,
		decimal: true
	},
	silicona585: {
		type: Number,
		decimal: true
	},
	T9: {
		type: Number,
		decimal: true
	}
});

Schemas.Produccion = new SimpleSchema({
	fechaProduccion: {
		type: Date
	},
	tipoDeBloque: {
		type: String
	},
	cantidad: {
		type: Number
	},
	poliol: {
		type: Number,
		decimal: true
	},
	copolimero: {
		type: Number,
		decimal: true
	},
	cloruro: {
		type: Number,
		decimal: true
	},
	tdi: {
		type: Number,
		decimal: true
	},
	amina: {
		type: Number,
		decimal: true
	},
	silicona595: {
		type: Number,
		decimal: true
	},
	silicona585: {
		type: Number,
		decimal: true
	},
	T9: {
		type: Number,
		decimal: true
	}
});

Insumos.attachSchema(Schemas.Insumo);
Producciones.attachSchema(Schemas.Produccion);
Stock.attachSchema(Schemas.Stock);