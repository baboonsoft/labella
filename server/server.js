// Insertar Stock en 0
if (Stock.find().count() === 0) {
  var stock = {
    nombreBloque: 0,
    poliol: 0,
    copolimero: 0,
    cloruro: 0,
    tdi: 0,
    amina: 0,
    silicona595: 0,
    silicona585: 0,
    T9: 0
  };
  Stock.insert(stock, {autoConvert: false}, function (error, resultStock) {
    if (error === null) {
      console.log("Stock ingresado: "+ resultStock);
    } else {
      console.log("Error al ingresar stock: " + error);
    }
  });
}
