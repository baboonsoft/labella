Router.route('/', function () {
  this.render('home');
});

Router.route('/insumos', function () {
  this.render('insumos');
});

Router.route('/producciones', function () {
  this.render('producciones');
});

Router.route('/stock', function () {
  this.render('stock', {
    data: function () {
     return Stock.findOne({});
    }
  });
});
